-- How much time does it take on average to reach the order_confirmation screen per session (in minutes)?
with order_confirmation_events as (
SELECT gse.fullvisitorid, gse.visitId, min(h.hitNumber) as min_hit_number
FROM `dhh-analytics-hiringspace.GoogleAnalyticsSample.ga_sessions_export` gse, gse.hit h, h.customDimensions as c
where c.value = 'order_confirmation'
group by 1, 2
)
SELECT AVG(h.time / 60000) as avg_reach_time
FROM `dhh-analytics-hiringspace.GoogleAnalyticsSample.ga_sessions_export` gse, gse.hit h
JOIN order_confirmation_events oce ON gse.fullvisitorid = oce.fullvisitorid AND gse.visitId = oce.visitId AND h.hitNumber = oce.min_hit_number;