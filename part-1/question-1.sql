-- How many sessions are there?
SELECT count(distinct concat(fullvisitorid, cast(visitId as string))) as sessions_count
FROM `dhh-analytics-hiringspace.GoogleAnalyticsSample.ga_sessions_export`;