
--    By using the GoogleAnalyticsSample data and BackendDataSample tables, analyse
--    how often users tend to change their location in the beginning of their journey (screens
--    like home and listing) versus in checkout and on order placement and demonstrate the
--    the deviation between earlier and later inputs (if any) in terms of coordinates change.
--    Then, using the BackendDataSample table, see if those customers who changed their
--    address ended placing orders and if those orders were delivered successfully, if so, did
--    they match their destination.

--- PART 1 query
 WITH raw_denormalized AS (SELECT fullvisitorid, visitId, visitStartTime, h.time AS hit_time, SAFE_CAST((SELECT value
                                                                                                  FROM UNNEST(h.customdimensions)
                                                                                                 WHERE index = 19
                                                                                                   AND value IS NOT NULL) AS FLOAT64) AS latitude,
                                   SAFE_CAST((SELECT value
                                      FROM UNNEST(h.customdimensions)
                                     WHERE index = 18 AND value IS NOT NULL) AS FLOAT64) AS longitude,
                                   (SELECT CASE WHEN value IN
                                                     ('home', 'shop_list', 'shop_details', 'search', 'product_details')
                                                    THEN 'earlier' ELSE 'later' END
                                      FROM UNNEST(h.customdimensions)
                                     WHERE index = 11
                                       AND value IN ('search', 'home', 'shop_details', 'shop_list', 'checkout',
                                                     'order_confirmation', 'product_details',
                                                     'Cancellation')) AS screen,
                                   (SELECT value FROM UNNEST(h.customdimensions) WHERE index = 36) AS transaction_id
                              FROM `dhh-analytics-hiringspace.GoogleAnalyticsSample.ga_sessions_export` gse,
                                   gse.hit h),
       raw_clean AS (SELECT fullvisitorid, visitId, visitStartTime, hit_time, latitude, longitude, screen,
                            transaction_id, RANK()
                                    OVER (PARTITION BY fullvisitorid, visitId, visitStartTime ORDER BY hit_time) AS hit_number
                       FROM raw_denormalized
                      WHERE latitude IS NOT NULL
                        AND longitude IS NOT NULL AND screen IS NOT NULL ),
final as (
SELECT cur.fullvisitorid, cur.visitId, cur.screen, cur.visitStartTime,
      ST_EQUALS(ST_GEOGPOINT(cur.longitude, cur.latitude ),ST_GEOGPOINT(prev.longitude, prev.latitude)) AS is_location_same,
       cur.transaction_id
  FROM raw_clean cur
           JOIN raw_clean prev
           ON cur.fullvisitorid = prev.fullvisitorid AND cur.visitid = prev.visitid AND prev.hit_number + 1 = cur.hit_number
           group by 1,2,3,4,5,6)
           select screen, is_location_same, count(distinct concat(fullvisitorid,visitid )) as user_count
           from final group by 1,2;

-- Result
--[
--  {
--    "screen": "later",
--    "is_location_same": true,
--    "user_count": "35555"
--  },
--  {
--    "screen": "later",
--    "is_location_same": false,
--    "user_count": "6586"
--  },
--  {
--    "screen": "earlier",
--    "is_location_same": true,
--    "user_count": "130842"
--  },
--  {
--    "screen": "earlier",
--    "is_location_same": false,
--    "user_count": "23446"
--  }
--]


--- PART 2
WITH raw_denormalized AS (SELECT fullvisitorid, visitId, visitStartTime, h.time AS hit_time, SAFE_CAST((SELECT value
                                                                                                  FROM UNNEST(h.customdimensions)
                                                                                                 WHERE index = 19
                                                                                                   AND value IS NOT NULL) AS FLOAT64) AS latitude,
                                   SAFE_CAST((SELECT value
                                      FROM UNNEST(h.customdimensions)
                                     WHERE index = 18 AND value IS NOT NULL) AS FLOAT64) AS longitude,
                                   (SELECT CASE WHEN value IN
                                                     ('home', 'shop_list', 'shop_details', 'search', 'product_details')
                                                    THEN 'earlier' ELSE 'later' END
                                      FROM UNNEST(h.customdimensions)
                                     WHERE index = 11
                                       AND value IN ('search', 'home', 'shop_details', 'shop_list', 'checkout',
                                                     'order_confirmation', 'product_details',
                                                     'Cancellation')) AS screen,
                                   (SELECT value FROM UNNEST(h.customdimensions) WHERE index = 36) AS transaction_id
                              FROM `dhh-analytics-hiringspace.GoogleAnalyticsSample.ga_sessions_export` gse,
                                   gse.hit h),
       raw_clean AS (SELECT fullvisitorid, visitId, visitStartTime, hit_time, latitude, longitude, screen,
                            transaction_id, RANK()
                                    OVER (PARTITION BY fullvisitorid, visitId, visitStartTime ORDER BY hit_time) AS hit_number
                       FROM raw_denormalized
                      WHERE latitude IS NOT NULL
                        AND longitude IS NOT NULL AND screen IS NOT NULL ),
dim_visitors_location_change as (
SELECT cur.fullvisitorid,
      cur.visitId,
      cur.screen,
      cur.visitStartTime,
      ST_EQUALS(ST_GEOGPOINT(cur.longitude, cur.latitude ),ST_GEOGPOINT(prev.longitude, prev.latitude)) AS is_location_same,
       cur.transaction_id,
       cur.longitude,
       cur.latitude
  FROM raw_clean cur
           JOIN raw_clean prev
           ON cur.fullvisitorid = prev.fullvisitorid AND cur.visitid = prev.visitid AND prev.hit_number + 1 = cur.hit_number
           group by 1,2,3,4,5,6,7,8)
SELECT
d_vlc.fullvisitorid,
CASE WHEN t.frontendOrderId IS NULL THEN FALSE ELSE TRUE END AS isOrderPlaced,
CASE WHEN t.status_id = 24 THEN TRUE ELSE FALSE END AS isOrderDelivered,
ST_EQUALS(ST_GEOGPOINT(d_vlc.longitude, d_vlc.latitude ),t.geopointDropoff) IsMatchedDestination
FROM dim_visitors_location_change d_vlc
LEFT JOIN `dhh-analytics-hiringspace.BackendDataSample.transactionalData` t ON t.frontendOrderId = d_vlc.transaction_id
WHERE transaction_id is not null AND is_location_same = false


