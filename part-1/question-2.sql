--How many sessions does each visitor create?
SELECT fullvisitorid, count(distinct concat(fullvisitorid, cast(visitId as string))) as sessions_count
FROM `dhh-analytics-hiringspace.GoogleAnalyticsSample.ga_sessions_export`
GROUP BY 1;


