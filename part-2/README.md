# milagros-cabra

There is dockerized api and `run.py` that works argument.

## Requirements

```bash
pip install -r requirements.txt
```

### Run

Run with argument:

```bash
usage: run.py [-h] --fullvisitorid FULLVISITORID
```

Run with Fast API:

```bash
docker build -t milagroscabra .
docker run -d --name milagroscabra_app -p 80:80 milagroscabra 
```
Endpoint:
`http://localhost/visitors/{fullvisitorid}`