import logging
import sys

from app.helpers import (get_fullvisitorid,
                         load_data,
                         get_frontend_order_id,
                         check_address_change,
                         is_order_delivered)

logger = logging.getLogger("app")

if __name__ == "__main__":
    # get args
    fullvisitorid = get_fullvisitorid()
    sessions_data = load_data("app/data/ga_sessions_export")
    transactional_data = load_data("app/data/transactionalData")
    user_session_data = sessions_data[sessions_data["fullvisitorid"] == fullvisitorid].compute()
    if len(user_session_data) == 0:
        logger.info("fullvisitorid couldn't find")
        print({"full_visitor_id": fullvisitorid,
               "address_changed": None,
               "is_order_placed": None,
               "is_order_delivered": None,
               "application_type": None,
               "msg": "fullvisitorid couldn't find"})
        sys.exit()
    # get user session
    last_session = user_session_data.nlargest(1, 'visitStartTime')
    hit_data = last_session.to_dict(orient='records')[0]["hit"]

    frontend_order_id = get_frontend_order_id(hit_data)
    # If there is no frontend_order_id then there is no order placed.
    if frontend_order_id:
        user_transaction_data = transactional_data[transactional_data["frontendOrderId"] == frontend_order_id].compute()
        print({"full_visitor_id": fullvisitorid,
               "address_changed": check_address_change(hit_data),
               "is_order_placed": not bool(len(user_transaction_data)),
               "is_order_delivered": is_order_delivered(user_session_data),
               "application_type": user_session_data["operatingSystem"].values[0]})
    else:
        print({"full_visitor_id": fullvisitorid,
               "address_changed": check_address_change(hit_data),
               "is_order_placed": False,
               "is_order_delivered": False,
               "application_type": user_session_data["operatingSystem"].values[0]})
