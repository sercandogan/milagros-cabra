import dask.dataframe as dd
from typing import Optional, Union
from numpy import ndarray
import glob
import os
import logging
import json
from argparse import ArgumentParser, Namespace

logger = logging.getLogger("helper")


def get_fullvisitorid() -> Namespace:
    """
    Argument parser for fullvisitorid
    :return: get string full visitor id
    """
    parser = ArgumentParser()
    parser.add_argument("--fullvisitorid", type=str, required=True)
    args = parser.parse_args()
    return args.fullvisitorid


def load_data(path: str) -> Optional[dd.DataFrame]:
    """
    Get customer data
    :param path: parquet data path
    :param fullvisitorid:
    :return: dd.DataFrame or None
    """
    file_list = glob.glob(os.path.join(path, "*"))
    if not file_list:
        logger.error(f"Files couldn't find: {path}")
        return

    try:
        df = dd.read_parquet(file_list, engine="pyarrow")
        return df
    except Exception as msg:
        logger.error(f"Reading error: {msg}")
        return


def get_value(data: ndarray, index: int) -> Optional[Union[str, int]]:
    """
    Get value from list(dict) with specific index
    :param data:
    :param index:
    :return:
    """
    for row in data:
        if row.get("index") == index:
            return row.get("value")
    return


def is_order_delivered(user_transactional_data: dd.DataFrame) -> bool:
    """
    If status_id on transactional data is 24 then order is delivered
    :param user_transactional_data: User's transactionals data
    :return: True or False
    """
    if user_transactional_data["status_id"].values[0] == 24:
        return True
    return False


def check_address_change(hit_data: list) -> bool:
    """
    Get all lat and lon data, if there are more than a lat or lon then location during the session is changed.
    :param hit_data: hit data is list(dict) that consist of hit fields and also custom dimensions.
    :return: True or False
    """
    lon_index = 18
    lat_index = 19

    number_of_lon_change = len(set(map(lambda hit: get_value(hit["customDimensions"], lon_index), hit_data)))
    number_of_lat_change = len(set(map(lambda hit: get_value(hit["customDimensions"], lat_index), hit_data)))

    return number_of_lon_change > 1 or number_of_lat_change > 1


def get_frontend_order_id(hit_data: list) -> Optional[str]:
    """
    Filter frontend order id from hit.customDimensions with index 36
    :param hit_data: hit data is list(dict) that consist of hit fields and also custom dimensions.
    :return: value(str) or None
    """
    frontend_order_index = 36
    for hit in hit_data:
        value = get_value(hit["customDimensions"], frontend_order_index)
        if value:
            return value
    return None


def response(status_code: int, body: dict) -> dict:
    """
    Response json helper

    :param status_code: status code of response
    :param body: response body
    :return: dict
    """
    return {
        "statusCode": status_code,
        "headers": {
            "Content-Type": "application/json"
        },
        "body": json.dumps(body)
    }
