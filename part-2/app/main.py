from typing import Optional
from app.helpers import (load_data,
                         get_frontend_order_id,
                         check_address_change,
                         is_order_delivered)
from fastapi import FastAPI
from fastapi.responses import Response
from fastapi.logger import logger

description = """
MilagrosCabraApp API helps you do awesome stuff. 🚀

## visitors

You will be able to:
*** get visitor last transaction status
"""
app = FastAPI(
    title="MilagrosCabraApp",
    description=description,
    version="0.0.1"
)

sessions_data = load_data("app/data/ga_sessions_export")
transactional_data = load_data("app/data/transactionalData")


@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.get("/visitors/{fullvisitorid}")
def get_visitor(fullvisitorid: int):
    fullvisitorid = str(fullvisitorid)
    try:
        user_session_data = sessions_data[sessions_data["fullvisitorid"] == fullvisitorid].compute()
    except:
        return Response(status_code=500, content={"msg": "internall data error"})

    if len(user_session_data) == 0:
        logger.info("fullvisitorid couldn't find")
        return Response(status_code=400, content={"full_visitor_id": fullvisitorid,
                                                  "address_changed": None,
                                                  "is_order_placed": None,
                                                  "is_order_delivered": None,
                                                  "application_type": None,
                                                  "msg": "fullvisitorid couldn't find"})
    # get user session
    last_session = user_session_data.nlargest(1, 'visitStartTime')
    hit_data = last_session.to_dict(orient='records')[0]["hit"]

    frontend_order_id = get_frontend_order_id(hit_data)
    # If there is no frontend_order_id then there is no order placed.
    if frontend_order_id:
        user_transaction_data = transactional_data[transactional_data["frontendOrderId"] == frontend_order_id].compute()
        return Response(status_code=200, content={"full_visitor_id": fullvisitorid,
                                                  "address_changed": check_address_change(hit_data),
                                                  "is_order_placed": not bool(len(user_transaction_data)),
                                                  "is_order_delivered": is_order_delivered(user_session_data),
                                                  "application_type": user_session_data["operatingSystem"].values[0]})
    else:
        return Response(status_code=200, content={"full_visitor_id": fullvisitorid,
                                                  "address_changed": check_address_change(hit_data),
                                                  "is_order_placed": False,
                                                  "is_order_delivered": False,
                                                  "application_type": user_session_data["operatingSystem"].values[0]})
