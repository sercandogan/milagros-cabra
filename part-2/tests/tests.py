from app.helpers import (get_value,
                         is_order_delivered,
                         check_address_change,
                         get_frontend_order_id)
import numpy as np
import dask.dataframe as dd
import pandas as pd


def test_get_value():
    data = np.array([{"index": 11, "value": "home"},
                     {"index": 36, "value": "sada-sadsd-s"}])

    assert get_value(data, 11) == "home"


def test_is_order_delivered():
    test_df = dd.from_pandas(pd.DataFrame([
        {
            "orderDate": "2019-03-01",
            "common_name": "Canada",
            "deliveryType": "OD",
            "backendOrderId": "3942376",
            "frontendOrderId": "s4mw-qdqu",
            "status_id": "16",
            "declinereason_code": "CUSTOMER_ONLINE_PAYMENT_PROBLEM",
            "declinereason_type": "CUSTOMER",
            "geopointCustomer": None,
            "geopointDropoff": None
        }
    ]), npartitions=1).compute()

    assert not is_order_delivered(test_df)


def test_check_address_change():
    test_hit_data = [

        {
            "hitNumber": "1",
            "time": "0",
            "hour": "0",
            "isInteraction": False,
            "isEntrance": None,
            "isExit": None,
            "type": "EVENT",
            "name": None,
            "landingScreenName": None,
            "screenName": None,
            "eventCategory": "ios.shop_list",
            "eventAction": "shop_list.loaded",
            "eventLabel": "H2X 2A7",
            "transactionId": None,
            "customDimensions": [
                {
                    "index": "18",
                    "value": "-73.57745610000001"
                },
                {
                    "index": "11",
                    "value": "shop_list"
                },
                {
                    "index": "15",
                    "value": "CA"
                },
                {
                    "index": "19",
                    "value": "45.5077126"
                },
                {
                    "index": "16",
                    "value": "Montreal"
                }
            ]
        }
    ]

    assert not check_address_change(test_hit_data)


def test_get_frontend_order_id():
    test_hit_data = [

        {
            "hitNumber": "1",
            "time": "0",
            "hour": "0",
            "isInteraction": False,
            "isEntrance": None,
            "isExit": None,
            "type": "EVENT",
            "name": None,
            "landingScreenName": None,
            "screenName": None,
            "eventCategory": "ios.shop_list",
            "eventAction": "shop_list.loaded",
            "eventLabel": "H2X 2A7",
            "transactionId": None,
            "customDimensions": [
                {
                    "index": "18",
                    "value": "-73.57745610000001"
                },
                {
                    "index": "11",
                    "value": "shop_list"
                },
                {
                    "index": "15",
                    "value": "CA"
                },
                {
                    "index": "19",
                    "value": "45.5077126"
                },
                {
                    "index": "16",
                    "value": "Montreal"
                }
            ]
        }
    ]
    assert get_frontend_order_id(test_hit_data) is None
